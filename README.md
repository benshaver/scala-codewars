## How to install SBT and Scala

First, get `SDKMAN!`:
- `$ curl -s "https://get.sdkman.io" | bash`

And install:
- `$ source ~/.sdkman/bin/sdkman-init.sh`

Use `sdkman` to install `sbt`:
- `$ sdk install sbt`

Run the `sbt` prompt (`exit` to quit):
- `$ sbt`
- `sbt:scala> `

To get a Scala REPL (`:q` to quit):
- `sbt:scala> console`

or
- `sbt console`

To compile:
- `sbt:scala> compile`

To run tests:
- `sbt:scala> test`

or
- `sbt test`

## Where to start?

- Read the [`sbt` by Example](https://www.scala-sbt.org/1.x/docs/sbt-by-example.html) from the `sbt` Getting Started Guide.
- Read the [Tour of Scala](https://docs.scala-lang.org/tour/tour-of-scala.html) on `scala-lang.org`
  - The [Scala Book](https://docs.scala-lang.org/overviews/scala-book/introduction.html) is a more in-depth reference
  - I recommend sticking to official Scala docs to get started. For example, by searching Google with the `site:scala-lang.org` option.
- I've started doing daily Code Wars "katas" in Scala. The ones I have completed I have added unit tests for. They're organized by the difficulty, or "kyu" of the Kata in question, which start at 8 and descend as they get more difficult. Obviously, these unit tests will fail for katas you have not completed yet. You can comment them out to focus on one at a time.
