package codewars

import org.scalatest.FunSuite

class FiveTestSuite extends FunSuite {
  // https://www.codewars.com/kata/5541f58a944b85ce6d00006a
  test("ProductFib.productFib test 1") {
      assert(ProductFib.productFib(4895L).sameElements(Array(55, 89, 1)))
  }
  test("ProductFib.productFib test 2") {
      assert(ProductFib.productFib(5895L).sameElements(Array(89, 144, 0)))
  }
  test("ProductFib.productFib test 3") {
      assert(ProductFib.productFib(74049690L).sameElements(Array(6765, 10946, 1)))
  }

  // https://www.codewars.com/kata/559a28007caad2ac4e000083
  test("SquaresPerimeter.squaresPerimeter test 1") {
      assert(SquaresPerimeter.squaresPerimeter(5) == 80)
  }
  test("SquaresPerimeter.squaresPerimeter test 2") {
      assert(SquaresPerimeter.squaresPerimeter(7) == 216)
  }

  // https://www.codewars.com/kata/561e9c843a2ef5a40c0000a4
  test("GapInPrimes.gapInPrimes test 1") {
      assert(GapInPrimes.gapInPrimes(2, 100, 100) == "")
  }
  test("GapInPrimes.gapInPrimes test 2") {
      assert(GapInPrimes.gapInPrimes(2, 100, 110) == "(101,103)")
  }
  test("GapInPrimes.gapInPrimes test 3") {
      assert(GapInPrimes.gapInPrimes(4, 100, 110) == "(103,107)")
  }
  test("GapInPrimes.gapInPrimes test 4") {
      assert(GapInPrimes.gapInPrimes(8, 359, 367) == "(359,367)")
  }
}
