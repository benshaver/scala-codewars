package codewars

import org.scalatest.FunSuite

class SixTestSuite extends FunSuite {
  // https://www.codewars.com/kata/54da5a58ea159efa38000836
  test("FindOdd.findOdd test 1") {
      assert(FindOdd.findOdd(List(20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5)) == 5)
  }

  // https://www.codewars.com/kata/541c8630095125aba6000c00
  test ("DigitalRoot.digitalRoot test 1") {
      assert(DigitalRoot.digitalRoot(16) == 7)
  }
  test ("DigitalRoot.digitalRoot test 2") {
      assert(DigitalRoot.digitalRoot(1) == 1)
  }
  test ("DigitalRoot.digitalRoot test 3") {
      assert(DigitalRoot.digitalRoot(493193) == 2)
  }

  // https://www.codewars.com/kata/554ca54ffa7d91b236000023
  test ("DeleteNth.deleteNth test 1") {
      assert(DeleteNth.deleteNth(List(20,37,20,21), 1) == List(20,37,21))
  }
  test ("DeleteNth.deleteNth test 2") {
      assert(DeleteNth.deleteNth(List(1,1,3,3,7,2,2,2,2), 3) == List(1, 1, 3, 3, 7, 2, 2, 2))
  }

  // https://www.codewars.com/kata/514b92a657cdc65150000006
  test ("MultiplesOf3Or5.multiplesOf3Or5 test 1") {
      assert(MultiplesOf3Or5.multiplesOf3Or5(10) == 23)
  }

  // https://www.codewars.com/kata/56bdd0aec5dc03d7780010a5
  test ("NextHigher.nextHigher test 1") {
      assert(NextHigher.nextHigher(129) == 130)
  }
  test ("NextHigher.nextHigher test 2") {
      assert(NextHigher.nextHigher(127) == 191)
  }
  test ("NextHigher.nextHigher test 3") {
      assert(NextHigher.nextHigher(1) == 2)
  }
  test ("NextHigher.nextHigher test 4") {
      assert(NextHigher.nextHigher(323423) == 323439)
  }

  // https://www.codewars.com/kata/5592e3bd57b64d00f3000047
  test ("FindNb.findNb test 1") {
      assert (FindNb.findNb(1071225L) == 45)
  }
  test ("FindNb.findNb test 2") {
      assert (FindNb.findNb(91716553919377L) == -1)
  }
  test ("FindNb.findNb test 3") {
      assert (FindNb.findNb(135440716410000L) == 4824)
  }

  // https://www.codewars.com/kata/5526fc09a1bbd946250002dc
  test ("FindOutlier.findOutlier test 1") {
      assert (FindOutlier.findOutlier(List(2, 4, 6, 8, 10, 3)) == 3)
  }
  test ("FindOutlier.findOutlier test 2") {
      assert (FindOutlier.findOutlier(List(2, 4, 0, 100, 4, 11, 2602, 36)) == 11)
  }
  test ("FindOutlier.findOutlier test 3") {
      assert (FindOutlier.findOutlier(List(160, 3, 1719, 19, 11, 13, -21)) == 160)
  }

  // https://www.codewars.com/kata/58d76854024c72c3e20000de
  test ("Reverse.reverse test 1") {
      assert  (Reverse.reverse("I really hope it works this time...") == "I yllaer hope ti works siht time...")
  }
  test ("Reverse.reverse test 2") {
      assert  (Reverse.reverse("Reverse this string, please!") == "Reverse siht string, !esaelp")
  }

  // https://www.codewars.com/kata/55b3425df71c1201a800009c
  test ("Stat.stat test 0a") {
      assert (Stat.range(List(1,3,-4, 5)) == 9)
  }
  test ("Stat.stat test 0b") {
      assert (Stat.mean(List(1,3,-4, 5)) == 1.25)
  }
  test ("Stat.test test 0c") {
      assert (Stat.median(List(1,3,-4, 5)) == 2)
  }
  test ("Stat.stat test 1") {
      assert (Stat.stat("01|15|59, 1|47|16, 01|17|20, 1|32|34, 2|17|17") == "Range: 01|01|18 Average: 01|38|05 Median: 01|32|34")
  }
  test ("Stat.stat test 2") {
      assert (Stat.stat("02|15|59, 2|47|16, 02|17|20, 2|32|34, 2|17|17, 2|22|00, 2|31|41") == "Range: 00|31|17 Average: 02|26|18 Median: 02|22|00")
  }
  test ("Stat.stat test 3") {
      assert (Stat.stat("") == "")
  }

  // https://www.codewars.com/kata/534d2f5b5371ecf8d2000a08
  test("MultiplicationTable.multiplicationTable test 1") {
      assert(MultiplicationTable.multiplicationTable(1) == List(List(1)))
  }
  test("MultiplicationTable.multiplicationTable test 2") {
      assert(MultiplicationTable.multiplicationTable(2) == List(List(1, 2), List(2, 4)))
  }
  test("MultiplicationTable.multiplicationTable test 3") {
      assert(MultiplicationTable.multiplicationTable(3) == List(List(1, 2, 3), List(2, 4, 6), List(3, 6, 9)))
  }

  // https://www.codewars.com/kata/550498447451fbbd7600041c
  test("CompSame.compSame test 1") {
      assert(CompSame.compSame(Seq(121, 144, 19, 161, 19, 144, 19, 11), Seq(11*11, 121*121, 144*144, 19*19, 161*161, 19*19, 144*144, 19*19)) == true)
  }
  test("CompSame.compSame test 2") {
      assert(CompSame.compSame(Seq(121, 144, 19, 161, 19, 144, 19, 11), Seq(11*11, 122*121, 144*144, 19*19, 161*161, 19*19, 144*144, 19*19)) == false)
  }
  test("CompSame.compSame test 3") {
      assert(CompSame.compSame(Seq(121, 144, 19, 161, 19, 144, 19, 11), null) == false)
  }

  // https://www.codewars.com/kata/550527b108b86f700000073f
  test("PiApprox.piApprox test 1") {
      assert(PiApprox.piApprox(0.1) == "[10, 3.0418396189]")
  }
  test("PiApprox.piApprox test 2") {
      assert(PiApprox.piApprox(0.01) == "[100, 3.1315929036]")
  }
  test("PiApprox.piApprox test 3") {
      assert(PiApprox.piApprox(0.001) == "[1000, 3.1405926538]")
  }

  // https://www.codewars.com/kata/56eb16655250549e4b0013f4
  test("MostFrequentDays.mostFrequentDays test 1") {
      assert(MostFrequentDays.mostFrequentDays(2427) == List("Friday"))
  }
  test("MostFrequentDays.mostFrequentDays test 2") {
      assert(MostFrequentDays.mostFrequentDays(2185) == List("Saturday"))
  }
  test("MostFrequentDays.mostFrequentDays test 3") {
      assert(MostFrequentDays.mostFrequentDays(1770) == List("Monday"))
  }
  test("MostFrequentDays.mostFrequentDays test 4") {
      assert(MostFrequentDays.mostFrequentDays(1785) == List("Saturday"))
  }
  test("MostFrequentDays.mostFrequentDays test 5") {
      assert(MostFrequentDays.mostFrequentDays(2860) == List("Thursday", "Friday"))
  }
}
