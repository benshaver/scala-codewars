package codewars

import org.scalatest.FunSuite

class SevenTestSuite extends FunSuite {
  // https://www.codewars.com/kata/5667e8f4e3f572a8f2000039
  test("Accum.accum test 1") {
      assert(Accum.accum("RqaEzty") == "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy")
  }

  // https://www.codewars.com/kata/555eded1ad94b00403000071/
  test("SeriesSum.seriesSum test 1") {
      assert(SeriesSum.seriesSum(0) == "0.00")
  }

  test("SeriesSum.seriesSum test 2") {
      assert(SeriesSum.seriesSum(1) == "1.00")
  }
  test("SeriesSum.seriesSum test 3") {
      assert(SeriesSum.seriesSum(2) == "1.25")
  }
  test("SeriesSum.seriesSum test 4") {
      assert(SeriesSum.seriesSum(5) == "1.57")
  }

  // https://www.codewars.com/kata/5a25ac6ac5e284cfbe000111
  test("ColouredTriangles.triangle test 1") {
      assert(ColouredTriangles.triangle("RRGBRGBB") == "G")
  }
  test("ColouredTriangles.triangle test 2") {
      assert(ColouredTriangles.triangle("RRR") == "R")
  }
  test("ColouredTriangles.triangle test 3") {
      assert(ColouredTriangles.triangle("G") == "G")
  }

  // https://www.codewars.com/kata/5acbc3b3481ebb23a400007d
  test("IsFlush.isFlush test 1") {
      assert(IsFlush.isFlush(List("AS", "3S", "9S", "KS", "4S")) == true)
  }
  test("IsFlush.isFlush test 2") {
      assert(IsFlush.isFlush(List("AD", "4S", "7H", "KC", "5S")) == false)
  }
  test("IsFlush.isFlush test 3") {
      assert(IsFlush.isFlush(List("AD", "4S", "10H", "KC", "5S")) == false)
  }
  test("IsFlush.isFlush test 4") {
      assert(IsFlush.isFlush(List("QD", "4D", "10D", "KD", "5D")) == true)
  }

  // https://www.codewars.com/kata/5d50e3914861a500121e1958
  test("AddLetters.addLetters test 1") {
      assert(AddLetters.addLetters(List('a', 'b', 'c')) == 'f')
  }
  test("AddLetters.addLetters test 2") {
      assert(AddLetters.addLetters(List('a', 'b')) == 'c')
  }
  test("AddLetters.addLetters test 3") {
      assert(AddLetters.addLetters(List('z')) == 'z')
  }
  test("AddLetters.addLetters test 4") {
      assert(AddLetters.addLetters(List('z', 'a')) == 'a')
  }
  test("AddLetters.addLetters test 5") {
      assert(AddLetters.addLetters(List('y', 'c', 'b')) == 'd')
  }
  test("AddLetters.addLetters test 6") {
      assert(AddLetters.addLetters(List()) == 'z')
  }

  // https://www.codewars.com/kata/5656b6906de340bd1b0000ac
  test("Longest.longest test 1") {
      assert(Longest.longest("aretheyhere", "yestheyarehere") == "aehrsty")
  }
  test("Longest.longest test 2") {
      assert(Longest.longest("loopingisfunbutdangerous", "lessdangerousthancoding") == "abcdefghilnoprstu")
  }
  test("Longest.longest test 3") {
      assert(Longest.longest("inmanylanguages", "theresapairoffunctions") == "acefghilmnoprstuy")
  }
}
